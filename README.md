Simple application Python Flask qui renvoie "Hello world!" sur le path /api/v1/hello
---

**Premier lancement :** 

Créons un venv, activons-le et installons quelques dépendances Python :
```
python -m venv .venv
source .venv/bin/activate
pip install flask
pip freeze > requirements.txt
export FLASK_APP=main.py

```

**Pour les lancements suivants :**

Nous nous contentons d'activer notre venv.
```
source .venv/bin/activate
pip install -r requirements.txt
export FLASK_APP=main.py
```
---
Excerice 1 : 
Mise en place 

Pour démarrer l'application en local : 
- Créer un fichier main.py comme dans hello-world1 
- Ajouter un fichier .gitignore comme dans hello-world1
- Créer un projet dans gitlab.com
- Initialiser un dépôt git
- Ajout un .gitignore comme celui présent dans ce dépôt
- Le pousser sur gitlab.com 

    ```
    flask run
    git init --initial-branch=main
    # Penser à adapter la commande suivante à votre dépôt.
    git remote add origin git@gitlab.com:cockpitio/team/nathanael/formation-cicd/hello-world-python.git
    git add main.py
    git add requirements.txt
    git commit -m "Initial commit"
    git push --set-upstream origin main
    ```

- Ouvrir le navigateur sur http://127.0.0.1:5000/api/v1/hello ou utiliser curl :

    ```
    curl http://127.0.0.1:5000//api/v1/hello -v
    *   Trying 127.0.0.1:5000...
    * Connected to 127.0.0.1 (127.0.0.1) port 5000
    > GET //api/v1/hello HTTP/1.1
    > Host: 127.0.0.1:5000
    > User-Agent: curl/8.6.0
    > Accept: */*
    > 
    < HTTP/1.1 200 OK
    < Server: Werkzeug/3.0.2 Python/3.11.8
    < Date: Tue, 09 Apr 2024 14:28:39 GMT
    < Content-Type: application/json
    < Content-Length: 25
    < Connection: close
    < 
    {"value":"Hello world!"}
    * Closing connection
    ```



---
Excercice 2 : 
Pipeline d'intégration continue : 

- Ajout d'un fichier .gitlab-ci.yaml qui lance le linter pylint, comme dans hello-world2
- Ajout de pylint dans requirements.txt

    ```
    pip install pylint
    pip freeze > requirements.txt
    ```

- Commit dans une nouvelle branche, pousser sur Gitlab, ouvrir une MR
    ```
    git checkout -b linter
    git add .gitlab-ci.yaml
    git commit
    git push --set-upstream origin linter
    ```
- Lancement automatique d'une pipeline sur gitlab.com
- Voir que la pipeline lance le linter, qui relève une erreur et échoue
- Créer une MR
- Forcer le succès des pipelines pour autoriser les fusions de MR, dans : 
    - Settings (au niveau du dépôt)
    - Merge requests
    - Cocher "Pipelines must succeed"
- Ajouter les docstring et une ligne vide en fin de fichier comme dans hello-world2 fichier main.py, commit et push
```diff
diff --git a/main.py b/main.py
index f2e9643..dde561b 100644
--- a/main.py
+++ b/main.py
@@ -1,9 +1,11 @@
+"""Simple Python Flask Hello World api."""
 from flask import Flask
 app = Flask(__name__)
 
 @app.route("/api/v1/hello")
 def hello():
+    """Function returning Hello world!"""
     return {"value": "Hello world!"}
 
 if __name__ == "__main__":
-    app.run(host='0.0.0.0', port=8000)
\ No newline at end of file
+    app.run(host='0.0.0.0', port=8000)
```
```
git add main.py
git commit
git push
```
- Le problème est corrigé, voir que la pipeline est à nouveau valide
- Accepter la MR, cliquer sur "Merge"

Pour aller plus loin : il faudrait ajouter d'autres types de tests (unitaires, fonctionnels) et analyses (sur le code, fuite de secret, sur les dépendances, générer un SBOM et l'analyser)

--- 
Excercice 3 : 
Livraison continue

Notre objectif est de créer un processus automatique qui teste, compile et crée un livrable de notre application. 
- Créons une nouvelle branche "build"
    ```
    git checkout -b build
    ```
- Ajoutons un Dockerfile (comme dans hello-world3)
- Ajoutons un stage build au fichier .gitlab-ci.yml (comme dans hello-world3)
- Voyons la pipeline CI/CD qui teste l'application, puis construit une image Docker et la publie sur le registre d'images de Gitlab (dans Deploy, Container Registry)
- Plaçons dans le rôle d'une personne chargée de déployer manuellement l'application et récupérons l'image en local
- Il nous faut pour cela un moyen de s'authentifier, tel qu'indiqué sur cette page https://docs.gitlab.com/ee/user/packages/container_registry/authenticate_with_container_registry.html En voici l'essentiel, en utilisant un personnal access token :
    You can create as many personal access tokens as you like.

        On the left sidebar, select your avatar.
        Select Edit profile.
        On the left sidebar, select Access Tokens.
        Select Add new token.
        Enter a name and expiry date for the token.
            The token expires on that date at midnight UTC.
            If you do not enter an expiry date, the expiry date is automatically set to 365 days later than the current date.
            By default, this date can be a maximum of 365 days later than the current date. 
        Select the desired scopes.
        Select Create personal access token. 

- Les commandes ci-dessous doivent être adaptées au projet de chacun

    ``` 
    docker login registry.gitlab.com -u username 
    docker pull registry.gitlab.com/cockpitio/team/nathanael/formation-cicd/hello-world-python
    ```
- Lançons l'application en local et vérifions son fonctionnement
    ```
    docker run -p 8000:8000 registry.gitlab.com/cockpitio/team/nathanael/formation-cicd/hello-world-python:latest
    curl 127.0.0.1:8000/api/v1/hello
    ```


