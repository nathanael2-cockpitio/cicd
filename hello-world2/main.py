"""Simple Python Flask Hello World api."""
from flask import Flask
app = Flask(__name__)

@app.route("/api/v1/hello")
def hello():
    """Function returning Hello world!"""
    return {"value": "Hello world!"}

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000)
